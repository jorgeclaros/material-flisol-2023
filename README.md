# Material FLISOL Comunidad YACUIBA LIBRE
1.- 2 Afiches para Redes Sociales

2.- Video para estados en Redes Sociales

3.- Menu de DVD para Software Libre para Windows.

4.- Documentación (Cartas de Invitacion a Medios de Comunicación, Modelos de Certicados).

5.- Material de Autopromocion para Expositores (Tarjetas de Presentación, Afiches de Servicios).

6.- Material Guia para FLISOL (Cartilla de Alternativas Libres).

7.- Triptico "Software Libre para Android".

8.- Juego Didactico Ruleta de Software Libre (con preguntas en Anki).

## Autor
Jorge Claros

## Licencia
GPL 3.0 (Software), Documentos (CC-BY).
