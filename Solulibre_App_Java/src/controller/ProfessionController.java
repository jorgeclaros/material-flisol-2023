package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.ProfessionModel;
import model.ProfessionDAO;
import view.ProfessionView;

public final class ProfessionController implements ActionListener {

    ProfessionDAO dao2 = new ProfessionDAO();
    ProfessionView vista2 = new ProfessionView();
    DefaultTableModel modelo2 = new DefaultTableModel();
    ProfessionModel pro = new ProfessionModel();

    public ProfessionController(ProfessionView p) {
        this.vista2 = p;
        this.vista2.btn_nuevo2.addActionListener(this);
        this.vista2.btn_nuevo_confirmar.addActionListener(this);
        this.vista2.btn_modificar2.addActionListener(this);
        this.vista2.btn_modificar_confirmar.addActionListener(this);
        this.vista2.btn_cancelar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista2.btn_nuevo_confirmar) {
            vista2.btn_nuevo2.setVisible(true);
            vista2.btn_modificar2.setVisible(true);
            vista2.btn_nuevo_confirmar.setVisible(false);
            vista2.btn_modificar_confirmar.setVisible(false);
            vista2.btn_cancelar.setVisible(false);
            vista2.Detalles2.setVisible(false);
            add();
            listar(vista2.tabla_profession);
            nuevo();
        }

        if (e.getSource() == vista2.btn_modificar2) {
            int fila = vista2.tabla_profession.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vista2, "Debe Seleccionar Una fila..!!");
            } else {
                vista2.btn_nuevo2.setVisible(false);
                vista2.btn_modificar2.setVisible(false);
                vista2.btn_nuevo_confirmar.setVisible(false);
                vista2.btn_modificar_confirmar.setVisible(true);
                vista2.btn_cancelar.setVisible(true);
                vista2.Detalles2.setVisible(true);
                int id = Integer.parseInt((String) vista2.tabla_profession.getValueAt(fila, 0).toString());
                String nom = (String) vista2.tabla_profession.getValueAt(fila, 1);
                String state = (String) vista2.tabla_profession.getValueAt(fila, 2);
                vista2.txt_id.setText("" + id);
                vista2.txt_name.setText(nom);
                vista2.combo_state.setSelectedItem(state);
            }
        }

        if (e.getSource() == vista2.btn_modificar_confirmar) {
            vista2.btn_nuevo2.setVisible(true);
            vista2.btn_modificar2.setVisible(true);
            vista2.btn_nuevo_confirmar.setVisible(false);
            vista2.btn_modificar_confirmar.setVisible(false);
            vista2.btn_cancelar.setVisible(false);
            vista2.Detalles2.setVisible(false);
            Actualizar();
            listar(vista2.tabla_profession);
            nuevo();
        }

        if (e.getSource() == vista2.btn_cancelar) {
            vista2.btn_nuevo2.setVisible(true);
            vista2.btn_modificar2.setVisible(true);
            vista2.btn_nuevo_confirmar.setVisible(false);
            vista2.btn_modificar_confirmar.setVisible(false);
            vista2.btn_cancelar.setVisible(false);
            vista2.Detalles2.setVisible(false);
            nuevo();
        }

        if (e.getSource() == vista2.btn_nuevo2) {
            vista2.btn_nuevo2.setVisible(false);
            vista2.btn_modificar2.setVisible(false);
            vista2.btn_nuevo_confirmar.setVisible(true);
            vista2.btn_modificar_confirmar.setVisible(false);
            vista2.btn_cancelar.setVisible(true);
            vista2.Detalles2.setVisible(true);
            nuevo();
        }

    }

    void nuevo() {
        vista2.txt_id.setText("");
        vista2.txt_name.setText("");
        vista2.txt_name.requestFocus();
    }

    public void listar(JTable tabla_profession) {
        modelo2 = (DefaultTableModel) tabla_profession.getModel();
        List<ProfessionModel> lista = dao2.listar();
        Object[] object = new Object[7];
        for (int i = 0; i < lista.size(); i++) {
            object[0] = lista.get(i).getidProfession();
            object[1] = lista.get(i).getName();
            if (lista.get(i).getState() == 1) {
                object[2] = "Habilitado";
            } else {
                object[2] = "Deshabilitado";
            }
            modelo2.addRow(object);
        }
        vista2.tabla_profession.setModel(modelo2);
    }

    public void Actualizar() {
        if (vista2.txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista2, "No se Identifica el ID debe selecionar la opcion Modificar");
        } else {
            int id = Integer.parseInt(vista2.txt_id.getText());
            String nom = vista2.txt_name.getText();
            int sta = 0;
            String combo = (String) vista2.combo_state.getSelectedItem();
            switch (combo) {
                case "Habilitado" ->
                    sta = 1;
                case "Deshabilitado" ->
                    sta = 0;
                default ->
                    JOptionPane.showMessageDialog(vista2, "Error al obtener Estado");
            }
            pro.setidProfession(id);
            pro.setName(nom);
            pro.setState(sta);
            int r = dao2.Actualizar(pro);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista2, "Profession modificado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista2, "Error");
            }
        }
        limpiarTabla();
    }

    void limpiarTabla() {
        for (int i = 0; i < vista2.tabla_profession.getRowCount(); i++) {
            modelo2.removeRow(i);
            i = i - 1;
        }
    }

    public void add() {
        String nom = vista2.txt_name.getText();
        int sta = 0;
        String combo = (String) vista2.combo_state.getSelectedItem();
        switch (combo) {
            case "Habilitado" ->
                sta = 1;
            case "Deshabilitado" ->
                sta = 0;
            default ->
                JOptionPane.showMessageDialog(vista2, "Error al obtener Estado");
        }
        pro.setName(nom);
        pro.setState(sta);
        int r = dao2.Agregar(pro);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista2, "Profession Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista2, "Error");
        }
        limpiarTabla();
    }
}
