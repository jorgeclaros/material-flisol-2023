package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.SoftwareModel;
import model.SoftwareDAO;
import view.SoftwareView;

public final class SoftwareController implements ActionListener {

    SoftwareDAO dao = new SoftwareDAO();
    SoftwareView vista = new SoftwareView();
    DefaultTableModel modelo = new DefaultTableModel();
    SoftwareModel soft = new SoftwareModel();

    public SoftwareController(SoftwareView v) {
        this.vista = v;
        this.vista.btn_nuevo.addActionListener(this);
        this.vista.btn_nuevo_confirmar.addActionListener(this);
        this.vista.btn_modificar.addActionListener(this);
        this.vista.btn_modificar_confirmar.addActionListener(this);
        this.vista.btn_cancelar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.btn_nuevo_confirmar) {
            vista.btn_nuevo.setVisible(true);
            vista.btn_modificar.setVisible(true);
            vista.btn_nuevo_confirmar.setVisible(false);
            vista.btn_modificar_confirmar.setVisible(false);
            vista.btn_cancelar.setVisible(false);
            vista.Detalles.setVisible(false);
            add();
            listar(vista.tabla_software);
            nuevo();
        }

        if (e.getSource() == vista.btn_modificar) {
            int fila = vista.tabla_software.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vista, "Debe Seleccionar Una fila..!!");
            } else {
                vista.btn_nuevo.setVisible(false);
                vista.btn_modificar.setVisible(false);
                vista.btn_nuevo_confirmar.setVisible(false);
                vista.btn_modificar_confirmar.setVisible(true);
                vista.btn_cancelar.setVisible(true);
                vista.Detalles.setVisible(true);
                int id = Integer.parseInt((String) vista.tabla_software.getValueAt(fila, 0).toString());
                String nom = (String) vista.tabla_software.getValueAt(fila, 1);
                String des = (String) vista.tabla_software.getValueAt(fila, 2);
                String iwin = (String) vista.tabla_software.getValueAt(fila, 3);
                String ilin = (String) vista.tabla_software.getValueAt(fila, 4);
                String guide = (String) vista.tabla_software.getValueAt(fila, 5);
                String state = (String) vista.tabla_software.getValueAt(fila, 6);
                vista.txt_id.setText("" + id);
                vista.txt_name.setText(nom);
                vista.txt_description.setText(des);
                vista.txt_inswin.setText(iwin);
                vista.txt_inslin.setText(ilin);
                vista.txt_guide.setText(guide);
                vista.combo_state.setSelectedItem(state);
            }
        }

        if (e.getSource() == vista.btn_modificar_confirmar) {
            vista.btn_nuevo.setVisible(true);
            vista.btn_modificar.setVisible(true);
            vista.btn_nuevo_confirmar.setVisible(false);
            vista.btn_modificar_confirmar.setVisible(false);
            vista.btn_cancelar.setVisible(false);
            vista.Detalles.setVisible(false);
            Actualizar();
            listar(vista.tabla_software);
            nuevo();
        }

        if (e.getSource() == vista.btn_cancelar) {
            vista.btn_nuevo.setVisible(true);
            vista.btn_modificar.setVisible(true);
            vista.btn_nuevo_confirmar.setVisible(false);
            vista.btn_modificar_confirmar.setVisible(false);
            vista.btn_cancelar.setVisible(false);
            vista.Detalles.setVisible(false);
            nuevo();
        }

        if (e.getSource() == vista.btn_nuevo) {
            vista.btn_nuevo.setVisible(false);
            vista.btn_modificar.setVisible(false);
            vista.btn_nuevo_confirmar.setVisible(true);
            vista.btn_modificar_confirmar.setVisible(false);
            vista.btn_cancelar.setVisible(true);
            vista.Detalles.setVisible(true);
            nuevo();
        }

    }

    void nuevo() {
        vista.txt_id.setText("");
        vista.txt_name.setText("");
        vista.txt_description.setText("");
        vista.txt_inswin.setText("");
        vista.txt_inslin.setText("");
        vista.txt_guide.setText("");
        vista.txt_name.requestFocus();
    }

    public void listar(JTable tabla_software) {
        modelo = (DefaultTableModel) tabla_software.getModel();
        List<SoftwareModel> lista = dao.listar();
        Object[] object = new Object[7];
        for (int i = 0; i < lista.size(); i++) {
            object[0] = lista.get(i).getIdSoftware();
            object[1] = lista.get(i).getName();
            object[2] = lista.get(i).getDescription();
            object[3] = lista.get(i).getInswin();
            object[4] = lista.get(i).getInslin();
            object[5] = lista.get(i).getGuide();
            //object[6] = lista.get(i).getState();
            if (lista.get(i).getState() == 1) {
                object[6] = "Habilitado";
            } else {
                object[6] = "Deshabilitado";
            }
            modelo.addRow(object);
        }
        vista.tabla_software.setModel(modelo);
    }

    public void Actualizar() {
        if (vista.txt_id.getText().equals("")) {
            JOptionPane.showMessageDialog(vista, "No se Identifica el ID debe selecionar la opcion Modificar");
        } else {
            int id = Integer.parseInt(vista.txt_id.getText());
            String nom = vista.txt_name.getText();
            String des = vista.txt_description.getText();
            String iwin = vista.txt_inswin.getText();
            String ilin = vista.txt_inslin.getText();
            String gde = vista.txt_guide.getText();
            int sta = 0;
            String combo = (String) vista.combo_state.getSelectedItem();
            switch (combo) {
                case "Habilitado" ->
                    sta = 1;
                case "Deshabilitado" ->
                    sta = 0;
                default ->
                    JOptionPane.showMessageDialog(vista, "Error al obtener Estado");
            }
            soft.setIdSoftware(id);
            soft.setName(nom);
            soft.setDescription(des);
            soft.setInswin(iwin);
            soft.setInslin(ilin);
            soft.setGuide(gde);
            soft.setState(sta);
            int r = dao.Actualizar(soft);
            if (r == 1) {
                JOptionPane.showMessageDialog(vista, "Software modificado con Exito.");
            } else {
                JOptionPane.showMessageDialog(vista, "Error");
            }
        }
        limpiarTabla();
    }

    void limpiarTabla() {
        for (int i = 0; i < vista.tabla_software.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }

    public void add() {
        String nom = vista.txt_name.getText();
        String des = vista.txt_description.getText();
        String iwin = vista.txt_inswin.getText();
        String ilin = vista.txt_inslin.getText();
        String gde = vista.txt_guide.getText();
        int sta = 0;
        String combo = (String) vista.combo_state.getSelectedItem();
        switch (combo) {
            case "Habilitado" ->
                sta = 1;
            case "Deshabilitado" ->
                sta = 0;
            default ->
                JOptionPane.showMessageDialog(vista, "Error al obtener Estado");
        }
        soft.setName(nom);
        soft.setDescription(des);
        soft.setInswin(iwin);
        soft.setInslin(ilin);
        soft.setGuide(gde);
        soft.setState(sta);
        int r = dao.Agregar(soft);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Software Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista, "Error");
        }
        limpiarTabla();
    }
}
