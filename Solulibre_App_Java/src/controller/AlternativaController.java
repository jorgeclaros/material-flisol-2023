package controller;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.AlternativaDAO;
import model.ProfessionModel;
import model.ProfessionDAO;
import model.SoftwareModel;
import model.SoftwareDAO;
import view.AlternativaView;

public final class AlternativaController implements ActionListener {

    ProfessionDAO daopro = new ProfessionDAO();
    SoftwareDAO daosoft = new SoftwareDAO();
    AlternativaDAO daoalt = new AlternativaDAO();
    AlternativaView vistaalt = new AlternativaView();
    DefaultTableModel modelo = new DefaultTableModel();

    public AlternativaController(AlternativaView p) {
        this.vistaalt = p;
        this.vistaalt.jComboBoxPro.addActionListener(this);
        this.vistaalt.jComboSoft.addActionListener(this);
        this.vistaalt.btn_nuevo.addActionListener(this);
        this.vistaalt.btn_eliminar.addActionListener(this);
        this.vistaalt.btn_reporte.addActionListener(this);
        this.vistaalt.btn_nuevo_confirmar.addActionListener(this);
        this.vistaalt.btn_cancelar.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String prof = (String) vistaalt.jComboBoxPro.getSelectedItem();
        if (e.getSource() == vistaalt.jComboBoxPro) {
            if (prof != "") {
                limpiarTabla();
                listarAlterntivasActivas(this.vistaalt.Tabla_Alternativas, prof);
            }
        }

        if (e.getSource() == vistaalt.btn_nuevo) {
            vistaalt.btn_nuevo.setVisible(false);
            vistaalt.btn_eliminar.setVisible(false);
            vistaalt.btn_reporte.setVisible(false);
            vistaalt.nueva_alt.setVisible(true);
            listarAlternativasActivas(vistaalt.jComboSoft, prof);
        }

        if (e.getSource() == vistaalt.btn_nuevo_confirmar) {
            String soft = (String) vistaalt.jComboSoft.getSelectedItem();
            if (soft != "") {
                limpiarTabla();
                add(prof, soft);
                listarAlterntivasActivas(vistaalt.Tabla_Alternativas, prof);
                vistaalt.btn_nuevo.setVisible(true);
                vistaalt.btn_eliminar.setVisible(true);
                vistaalt.btn_reporte.setVisible(true);
                vistaalt.nueva_alt.setVisible(false);

            }

        }

        if (e.getSource() == vistaalt.btn_cancelar) {
            vistaalt.btn_nuevo.setVisible(true);
            vistaalt.btn_eliminar.setVisible(true);
            vistaalt.btn_reporte.setVisible(true);
            vistaalt.nueva_alt.setVisible(false);
        }

        if (e.getSource() == vistaalt.btn_eliminar) {
            int fila = vistaalt.Tabla_Alternativas.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(vistaalt, "Debes Seleccionar Una fila");
            } else {
                int id = Integer.parseInt((String) vistaalt.Tabla_Alternativas.getValueAt(fila, 0).toString());
                delete(prof, id);
                listarAlterntivasActivas(vistaalt.Tabla_Alternativas, prof);
            }
        }

        if (e.getSource() == vistaalt.btn_reporte) {
            //delete();
            //listar(vista.tabla);
        }
    }

    public void add(String pro, String sft) {
        int r = daoalt.agregar(pro,sft);
        if (r == 1) {
            JOptionPane.showMessageDialog(vistaalt, "Alternativa agregada con Exito.");
        } else {
            JOptionPane.showMessageDialog(vistaalt, "Error al agregar Alternativa");
        }
        limpiarTabla();
    }

    public void delete(String pro, int idsoft) {
        daoalt.delete(pro, idsoft);
        JOptionPane.showMessageDialog(vistaalt, "Alternativa Eliminada");
        limpiarTabla();
    }

    public void listarAlterntivasActivas(JTable Tabla_Alternativas, String pro) {
        modelo = (DefaultTableModel) Tabla_Alternativas.getModel();
        List<SoftwareModel> lista = daosoft.listarAlterntivas(pro);
        if (!lista.isEmpty()) {
            Object[] object = new Object[6];
            for (int i = 0; i < lista.size(); i++) {
                object[0] = lista.get(i).getIdSoftware();
                object[1] = lista.get(i).getName();
                object[2] = lista.get(i).getDescription();
                object[3] = lista.get(i).getInswin();
                object[4] = lista.get(i).getInslin();
                object[5] = lista.get(i).getGuide();
                modelo.addRow(object);
            }
        } else {
            JOptionPane.showMessageDialog(vistaalt, "No existen alternativas para esta profesion", "Error", JOptionPane.ERROR_MESSAGE);
        }
        vistaalt.Tabla_Alternativas.setModel(modelo);
    }

    public void listarProfesionesActivas(JComboBox jComboBoxPro) {
        try {
            List<ProfessionModel> professions = daopro.listarProfesionesActivas();
            if (!professions.isEmpty()) {
                DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<>();
                for (ProfessionModel profession : professions) {
                    comboBoxModel.addElement(profession.getName());
                }
                vistaalt.getComboBox().setModel(comboBoxModel);
            } else {
                JOptionPane.showMessageDialog(vistaalt, "No hay profesiones disponibles", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Error al obtener las profesiones: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void listarAlternativasActivas(JComboBox jComboBoxSoft, String pro) {
        try {
            List<SoftwareModel> softwares = daosoft.listarAlterntivasAgregar(pro);
            if (!softwares.isEmpty()) {
                DefaultComboBoxModel<String> comboBoxModel = new DefaultComboBoxModel<>();
                for (SoftwareModel software : softwares) {
                    comboBoxModel.addElement(software.getName());
                }
                vistaalt.getComboBoxSoft().setModel(comboBoxModel);
            } else {
                JOptionPane.showMessageDialog(vistaalt, "No hay Software disponibles", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (HeadlessException e) {
            JOptionPane.showMessageDialog(null, "Error al obtener los software: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    void limpiarTabla() {
        for (int i = 0; i < vistaalt.Tabla_Alternativas.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }

    /*
    public void add() {
        String nom = vista1.txt_name.getText();
        int sta = Integer.parseInt(vista1.txt_state.getText());
        pro.setName(nom);
        pro.setState(sta);
        int r = dao3.Agregar(pro);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista1, "Alternativa Agregado con Exito.");
        } else {
            JOptionPane.showMessageDialog(vista1, "Error");
        }
        limpiarTabla();
    }
     */
}
