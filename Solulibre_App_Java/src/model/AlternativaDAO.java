package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class AlternativaDAO {

    ConnectDB connect = new ConnectDB();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    

    public int agregar(String pro, String sft) {
        int r = 0;
        try {
            con = connect.getConnection();
            String sql = "INSERT INTO Profession_has_Software (idProfession,idSoftware) VALUES ((SELECT idProfession FROM Profession WHERE name = ?),(SELECT idSoftware FROM Software WHERE name = ?))";
            ps = con.prepareStatement(sql);
            ps.setString(1, pro);
            ps.setString(2, sft);
            r = ps.executeUpdate();
        } catch (SQLException e) {
        }
        return r;
    }

    public int delete(String pro, int idsoft) {
        int r = 0;
        try {
            con = connect.getConnection();
            String sql = "DELETE FROM Profession_has_Software WHERE idProfession = (SELECT idProfession FROM Profession WHERE name = ?) AND idSoftware = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, pro);
            ps.setInt(2, idsoft);
            r = ps.executeUpdate();
        } catch (SQLException e) {
        }
        return r;
    }

    public List listar() {
        List<AlternativaModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT * FROM Profession_has_Software";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                AlternativaModel alt = new AlternativaModel();
                alt.setIdProfession(rs.getInt("idProfession"));
                alt.setIdSoftware(rs.getInt("idSoftware"));
                datos.add(alt);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }
}
