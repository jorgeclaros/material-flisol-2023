/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class AlternativaModel {
    int idSoftware;
    int idProfession;
    
    public AlternativaModel() {   
    }

    public AlternativaModel(int idSoftware, int idProfession) {
        this.idSoftware = idSoftware;
        this.idProfession=idProfession;
    }
    
     public int getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(int idSoftware) {
        this.idSoftware = idSoftware;
    }
    
    public int getIdProfession() {
        return idProfession;
    }

    public void setIdProfession(int idProfession) {
        this.idProfession = idProfession;
    }
}