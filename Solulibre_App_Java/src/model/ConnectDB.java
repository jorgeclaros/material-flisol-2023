/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
    Connection connection = null;
    
    public Connection getConnection() throws SQLException {
        try {
            File dbfile=new File(".");
            connection = DriverManager.getConnection("jdbc:sqlite:"+dbfile.getAbsolutePath()+"/solulibre.db");
            
        } catch (SQLException e) {
            System.err.println("Error al conectar con la base de datos: " + e.getMessage());
        }
        return connection;
    }
}