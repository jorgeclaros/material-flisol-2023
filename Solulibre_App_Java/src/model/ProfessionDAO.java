package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class ProfessionDAO {

    ConnectDB connect = new ConnectDB();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public List listarProfesionesActivas() {
       List<ProfessionModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT idProfession, name FROM Profession WHERE state = ? ORDER BY name";
            ps = con.prepareStatement(sql);
            ps.setString(1, "1");
            rs = ps.executeQuery();
            while (rs.next()) {
                ProfessionModel pro = new ProfessionModel();
                pro.setidProfession(rs.getInt("idProfession"));
                pro.setName(rs.getString("name"));
                datos.add(pro);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }

    public List listar() {
        List<ProfessionModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT * FROM Profession ORDER BY name";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                ProfessionModel pro = new ProfessionModel();
                pro.setidProfession(rs.getInt("idProfession"));
                pro.setName(rs.getString("name"));
                pro.setState(rs.getInt("state"));
                datos.add(pro);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }

    public int Actualizar(ProfessionModel prof) {
        int upd = 0;
        try {
            con = connect.getConnection();
            String sql = "UPDATE Profession SET name= ?, state= ? WHERE idProfession = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, prof.getName());
            ps.setInt(2, prof.getState());
            ps.setInt(3, prof.getidProfession());
            upd = ps.executeUpdate();
            if (upd == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException e) {
        }
        return upd;
    }

    public int Agregar(ProfessionModel prof) {
        int ins = 0;
        try {
            con = connect.getConnection();
            String sql = "INSERT INTO Profession (name, state) values (?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, prof.getName());
            ps.setInt(2, prof.getState());
            ins = ps.executeUpdate();
            if (ins == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException e) {
        }
        return ins;
    }
}
