/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class ProfessionModel {
    int idProfession;
    String name;
    int state;
    
    public ProfessionModel() {   
    }

    public ProfessionModel(int idProfession, String name, int state) {
        this.idProfession = idProfession;
        this.name=name;
        this.state=state;
    }
    
     public int getidProfession() {
        return idProfession;
    }

    public void setidProfession(int idProfession) {
        this.idProfession = idProfession;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }    
}