/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

public class SoftwareModel {
    int idSoftware;
    String name;
    String description;
    String inswin;
    String inslin;
    String guide;
    int state;
    
    public SoftwareModel() {   
    }

    public SoftwareModel(int idSoftware, String name, String description, String inswin, String inslin, String guide, int state) {
        this.idSoftware = idSoftware;
        this.name=name;
        this.description=description;
        this.inswin=inswin;
        this.inslin=inslin;
        this.guide=guide;       
        this.state=state;
    }
    
     public int getIdSoftware() {
        return idSoftware;
    }

    public void setIdSoftware(int idSoftware) {
        this.idSoftware = idSoftware;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getInswin() {
        return inswin;
    }

    public void setInswin(String inswin) {
        this.inswin = inswin;
    }    
    
    public String getInslin() {
        return inslin;
    }

    public void setInslin(String inslin) {
        this.inslin = inslin;
    }        
    
    public String getGuide() {
        return guide;
    }

    public void setGuide(String guide) {
        this.guide = guide;
    }
    
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }    
}