package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class SoftwareDAO {

    ConnectDB connect = new ConnectDB();
    SoftwareModel sftm = new SoftwareModel();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    public List listar() {
        List<SoftwareModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT * FROM Software ORDER BY name";
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                SoftwareModel sft = new SoftwareModel();
                sft.setIdSoftware(rs.getInt("idSoftware"));
                sft.setName(rs.getString("name"));
                sft.setDescription(rs.getString("description"));
                sft.setInswin(rs.getString("inswin"));
                sft.setInslin(rs.getString("inslin"));
                sft.setGuide(rs.getString("guide"));
                sft.setState(rs.getInt("state"));
                datos.add(sft);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }

    public List listarAlterntivas(String pro) {
        List<SoftwareModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT a.idSoftware, a.name,a.description,a.inswin,a.inslin,a.guide FROM Software AS a, Profession_has_Software AS b WHERE b.idProfession=(SELECT idProfession FROM Profession WHERE name = ?) AND b.idSoftware=a.idSoftware ORDER BY a.name";
            ps = con.prepareStatement(sql);
            ps.setString(1, pro);
            rs = ps.executeQuery();

            while (rs.next()) {
                SoftwareModel sft = new SoftwareModel();
                sft.setIdSoftware(rs.getInt("idSoftware"));
                sft.setName(rs.getString("name"));
                sft.setDescription(rs.getString("description"));
                sft.setInswin(rs.getString("inswin"));
                sft.setInslin(rs.getString("inslin"));
                sft.setGuide(rs.getString("guide"));
                datos.add(sft);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }
    
    public List listarAlterntivasAgregar(String pro) {
        List<SoftwareModel> datos = new ArrayList<>();
        try {
            con = connect.getConnection();
            String sql = "SELECT name FROM Software WHERE idSoftware NOT IN (SELECT idSoftware FROM Profession_has_Software WHERE idProfession=(SELECT idProfession FROM Profession WHERE name = ?)) ORDER BY name";
            ps = con.prepareStatement(sql);
            ps.setString(1, pro);
            rs = ps.executeQuery();
            while (rs.next()) {
                SoftwareModel sft = new SoftwareModel();
                sft.setName(rs.getString("name"));
                datos.add(sft);
            }
            con.close();
        } catch (SQLException e) {
            System.out.println("Exception: " + e);
        }
        return datos;
    }

    public int Actualizar(SoftwareModel soft) {
        int upd = 0;
        try {
            con = connect.getConnection();
            String sql = "UPDATE Software SET name= ?, description= ?, inswin= ?, inslin= ?, guide= ?, state= ? WHERE idSoftware = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, soft.getName());
            ps.setString(2, soft.getDescription());
            ps.setString(3, soft.getInswin());
            ps.setString(4, soft.getInslin());
            ps.setString(5, soft.getGuide());
            ps.setInt(6, soft.getState());
            ps.setInt(7, soft.getIdSoftware());
            upd = ps.executeUpdate();
            if (upd == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException e) {
        }
        return upd;
    }

    public int Agregar(SoftwareModel soft) {
        int ins = 0;
        try {
            con = connect.getConnection();
            String sql = "INSERT INTO Software (name, description, inswin, inslin, guide, state) values (?,?,?,?,?,?)";
            ps = con.prepareStatement(sql);
            ps.setString(1, soft.getName());
            ps.setString(2, soft.getDescription());
            ps.setString(3, soft.getInswin());
            ps.setString(4, soft.getInslin());
            ps.setString(5, soft.getGuide());
            ps.setInt(6, soft.getState());
            ins = ps.executeUpdate();
            if (ins == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException e) {
        }
        return ins;
    }
}
