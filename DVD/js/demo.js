/*
Funciones JS de menu de DVD
Author: Jorge A. Claros L.
*/

window.onload = function(){
	de=document.getElementById("det");
	lo=document.getElementById("logo");
	list=[];
	count=0;
}

function install(){
	for(i=1;i<=29;i++){
		sel = document.getElementById("o"+i);
		if(sel.checked){
			list.push(sel.name);
		}
	}
	if(list.length>0){
		doing();
	} else {
		alert("Selecciones minimo 1 opcion");
	}
}

function doing(){
	if (count<list.length){
		var oShell = new ActiveXObject("Shell.Application");
		if(list[count] == "o2" || list[count] == "o6" || list[count] == "o7" ||  list[count] == "o8" ||  list[count] == "o11" || list[count] == "o17" || list[count] == "o19" || list[count] == "o28"){
			var prog = "data\\"+list[count]+".exe";
			oShell.ShellExecute(prog,"/SILENT","","open","1");
		} else if (list[count] == "o5" || list[count] == "o9" || list[count] == "o12" || list[count] == "o18" || list[count] == "o20" || list[count] == "o21" || list[count] == "o24") {
			var prog = "data\\"+list[count]+".exe";
			oShell.ShellExecute(prog,"/S","","open","1");
		} else if (list[count] == "o3" || list[count] == "o4" || list[count] == "o13" || list[count] == "o25") {
			var prog = "MsiExec.exe";
			oShell.ShellExecute(prog,"/i data\\"+list[count]+".msi /passive RebootYesNo=No","","open","1");
		} else {
			// Opcion Nula para Portables o Exe "No SILENT"
		}
		count++;
		setTimeout(doing, 7000);	
	} else {
		alert("Instalacion Finalizada");
		list=[];
		count=0;		
	}
}

function Descr(a){
		switch(a){			
            case 1:
				de.value = "Traduce textos de Español a Ingles y viceversa, con este util diccionario";
				lo.src="images/logos/babiloo.png";
				break;
            case 2:	
				de.value = "Navega por internet con velocidad y seguridad.";
				lo.src="images/logos/firefox.png";
				break;
            case 3:	
				de.value = "Crea Documentos, Hojas de Calculos, Presentaciones y demas. ALTERNATIVA A: Microsoft Office.";
				lo.src="images/logos/libreoffice.png";
				break;
            case 4:	
				de.value = "Gestiona tus correos electronicos Gmail, Outlook, Yahoo, etc.";
				lo.src="images/logos/thunderbird.png";
				break;
            case 5:	
				de.value = "Crea Impresionantes Presentaciones Dinamicas sin Internet. ALTERNATIVA A: Prezi.";
				lo.src="images/logos/sozi.png";
				break;
            case 6:	
				de.value = "Haz Citas y Bibliografia con Normas APA, Vancouver y demas.";
				lo.src="images/logos/zotero.png";
				break;
            case 7:	
				de.value = "Edita Archivos de audio con esta herramienta muy simple y copleta. ALTERNATIVA A: Sony SOundforge,  Adobe Audition";
				lo.src="images/logos/audacity.png";
				break;
            case 8:	
				de.value = "Edita Imagenes para crear Publicidades, Panfletos, Infografias, etc. ALTERNATIVA A: Adobe Photoshop";
				lo.src="images/logos/gimp.png";
				break;
			case 9:
				de.value = "Edita Videos con esta completa herramienta que tiene gran variedad de efectos y filtros. ALTERNATIVA A: Adobe Premier";
				lo.src="images/logos/kdelive.png";
				break;
			case 10:
				de.value = "Agrega efectos espectaculares a tus videos. ALTERNATIVA A: Adobe AfterEffects";
				lo.src="images/logos/natron.png";
				break;		
			case 11:
				de.value = "Convierte entre diferentes formatos de Video y Audio con solo unos cuantos clics. ALTERNATIVA A: Total Video Converter";
				lo.src="images/logos/winff.png";
				break;
			case 12:
				de.value = "Ve videos, haz streaming, prueba tu camara y demas con este Reproductor de Video Multifuncional. ALTERNATIVA A: PowerDVD";
				lo.src="images/logos/vlc.png";
				break;
			case 13:
				de.value = "Programa tu placa Arduino con Este IDE para Robotica.";
				lo.src="images/logos/arduino.png";
				break;
			case 14:
				de.value = "Crea Diagramas Elctronicos y Diseños PCB de manera simple y rapida.";
				lo.src="images/logos/fritzing.png";
				break;				
			case 15:
				de.value = "Audita la seguridad de tu Red con esta herramienta esencial de seguridad informatica.";
				lo.src="images/logos/nmap.png";
				break;				
			case 16:
				de.value = "Crea tu laboratorio de para probar diferentes Sistemas Operativos. ALTERNATIVA A: VMWare";
				lo.src="images/logos/virtualbox.png";
				break;					
			case 17:
				de.value = "Crea Codigo con Javascript, Python, Java o PHP. ALTERNATIVA A: Sublime Text";
				lo.src="images/logos/vscode.png";
				break;
			case 18:
				de.value = "Audita la seguridad de tu Red con esta herramienta esencial de seguridad informatica.";
				lo.src="images/logos/wireshark.png";
				break;
			case 19:
				de.value = "Gestiona la Contabilidad de tu Microempresa o PYMES.";
				lo.src="images/logos/gnucash.png";
				break;
			case 20:
				de.value = "Gestiona la Contabilidad de Asociaciones o Sindicatos de manera simple.";
				lo.src="images/logos/grisbi.png";
				break;
			case 21:
				de.value = "Digitaliza tus encuestas y ve con simplicidad las estadisticas. ALTERNATIVA A: PSPP";
				lo.src="images/logos/pspp.png";
				break;
			case 22:
				de.value = "Diseña planos hidricos con esta Potente herramienta.";
				lo.src="images/logos/hechms.png";
				break;					
			case 23:
				de.value = "Diseña planos hidricos con esta herramienta que muy liviana.";
				lo.src="images/logos/hecras.png";
				break;
			case 24:
				de.value = "Diseña Planos 2D de todo tipo con esta herramienta que es muy liviana. ALTERNATIVA A: Autodesk AutoCAD";
				lo.src="images/logos/librecad.png";
				break;
			case 25:
				de.value = "Diseña los planos infomacion geografica con esta gran herramienta. ALTERNATIVA A: ArcGIS"
				lo.src="images/logos/qgis.png";
				break;				
            case 26:	
				de.value = "Limpia los archivos temporales de tu PC y opten mas espacio.  ALTERNATIVA A: Ccleaner.";
				lo.src="images/logos/bleachbit.png";
				break;
            case 27:	
				de.value = "Recupera el manu inicio de Windows, en Windows 8";
				lo.src="images/logos/openshell.png";	
				break;
            case 28:	
				de.value = "COmprime y Decomprime Archivos de tu Equipo. ALTERNATIVA A: Winrar.";
				lo.src="images/logos/peazip.png";
				break;
            case 29:	
				de.value = "Crea Unidades Booteables con tu Memoria Flash.";
				lo.src="images/logos/rufus.png";
				break;
			dafault:
				de.value = "";
			}	
	}
	
function msg(soft) {
		var oShell = new ActiveXObject("Shell.Application");
		var prog = "data\\"+soft;
  		oShell.ShellExecute(prog,"","","open","1");
	}
	
function OneMsi(e){
	var msi=['o3','o4','o13','o25'];
		alert("Solo Puedes elegir 1 paquete MSI a la vez: Arduino, LibreOffice, QGIS o Thunderbird. Pero puedes instalarlo manualmente");
		for(i=0;i<msi.length;i++){
			sel=document.getElementById(msi[i]);
			if(e==msi[i]){
				sel.checked=true;
			} else {
				sel.checked=false;
			}
		}
	}
 
